from django.contrib import admin
from django.contrib.auth.models import Group
from .models import Account
# Register your models here.


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone', 'created_at', 'gender', 'age', 'educational_institution',
                    'stars')
    list_filter = ('first_name', 'last_name', 'phone', 'created_at', 'gender', 'age', 'educational_institution',
                   'stars')


admin.site.unregister(Group)
admin.site.register(Account, ProfileAdmin)
