from rest_framework import serializers
from .models import Account


class AccountSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Account
        fields = (
            'id', 'phone', 'first_name', 'last_name',
            'created_at', 'gender', 'age', 'educational_institution',
        )
        read_only_fields = ('created_at', 'stars', 'id')
