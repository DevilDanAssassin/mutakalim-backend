from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.
from rest_framework.exceptions import ValidationError

GENDER_CHOICES = (
    ('М', 'Мужской'),
    ('Ж', 'Женский'),
)


class Account(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=60)
    last_name = models.CharField(verbose_name='Фамилия', max_length=60)
    phone = PhoneNumberField(verbose_name='Телефон', unique=True)
    created_at = models.DateTimeField(verbose_name='Дата Создания', auto_now_add=True)
    gender = models.CharField(verbose_name='Пол', max_length=1, choices=GENDER_CHOICES)
    age = models.IntegerField(verbose_name='Возраст', null=True)
    educational_institution = models.CharField(verbose_name='Учебное заведение', max_length=60)
    stars = models.IntegerField(verbose_name='Звезды', default=0)


    class Meta:
        verbose_name = 'Игрок'
        verbose_name_plural = 'Игроки'

