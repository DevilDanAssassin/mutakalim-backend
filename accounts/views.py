from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from accounts.models import Account
from accounts.serializers import AccountSerializer
from django.views.decorators.csrf import csrf_exempt


@api_view(['GET', 'POST'])
def create_profile(request):
    if request.method == 'GET':
        accounts = Account.objects.all()
        serializer = AccountSerializer(accounts, many=True)
        return Response(serializer.data)

    if request.method == 'POST':
        serializer = AccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data['id'], status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def add_stars(request, pk):
    if request.method == 'PUT':
        profile = get_object_or_404(Account, id=pk)
        profile.stars += int(request.data['stars'])
        profile.save()
    return Response(profile.id)
