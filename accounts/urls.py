from django.urls import path
from accounts import views


# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('profile/', views.create_profile),
    path('add/<int:pk>/', views.add_stars)
]